#!/bin/bash
# Copyright 2017 Noah Meyerhans
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# This script can be used to launch an instance for testing. It's
# mostly just a convenience to avoid having to type in a lot of
# command-line boilerplate.

usage() {
    cat <<EOF
$0 [OPTIONS]
OPTIONS:
  -p, --profile            Use the given AWS profile for API calls
  -a, --ami                Override the default builder AMI
  -t, --instance-type      Override the default builder instance type
  -C, --classlist          Override the list of classes used by FAI
  -b, --fai-config-branch  Use the given branch in the FAI config git repo
  -h, --help               Show usage
EOF
}

bin_dir=$(dirname $0)
share_dir=${SHARE_DIR:-${bin_dir}/../share/templates}

userdata_final=$(mktemp) || exit 1
envfile=$(mktemp) || exit 1

trap 'rm -f "$userdata_final" "$envfile"' EXIT

TEMP=$(getopt -o 'p:a:t:C:b:h' \
              --long 'profile:,ami:,instance-type:,fai-config-branch:,classlist:,help' \
              -n "$0" -- "$@")

if [ $? -ne 0 ]; then
    echo 'Terminating...' >&2
    exit 1
fi

eval set -- "$TEMP"
unset TEMP

while true ; do
    case "$1" in
        -p|--profile)
            PROFILE="$2" ; shift 2
            ;;
        -a|--ami)
            builder_ami_override="--image-id $2" ; shift 2
            ;;
        -t|--instance-type)
            builder_instance_type="--instance-type $2"; shift 2
            ;;
        -b|--fai-config-branch)
            fai_config_branch="$2" ; shift 2
            ;;
        -C|--classlist)
            fai_class_list="$2" ; shift 2
            ;;
        -h|--help)
            usage; shift
            exit 0
            ;;
        --)
            shift ; break
            ;;
        *)
            echo "That's not right" >&2
            exit 1
            ;;
    esac
done

command -v jq > /dev/null 2>&1 || {
    echo "jq not found in PATH" >&2
    exit 1
}
command -v aws > /dev/null 2>&1 || {
    echo "aws not found in PATH" >&2
    exit 1
}

cp "${share_dir}/ec2-userdata.yaml" "$userdata_final"

for var in fai_class_list fai_config_branch fai_config_repo; do
    val=$(eval echo \$$var)
    if [ -n "$val" ]; then
        echo "$var=${val@Q}" >> "$envfile"
    fi
done

if [ -s "$envfile" ]; then
    env_md5=$(cat "$envfile" | openssl base64 -A)
    sed -i "s/content: IyBlbXB0eQo=/content: $env_md5/" $userdata_final
fi

instance_id=$(aws --profile $PROFILE --region us-east-1 --output json \
                  ec2 run-instances $builder_instance_type \
                  $builder_ami_override \
                  --cli-input-json file://${share_dir}/debian-acct.json \
                  --user-data file://${userdata_final} | jq -r .Instances[0].InstanceId)
if [ -z "$instance_id" ]; then
    echo "Didn't get back an instance ID. Nothing launched?" >&2
    exit 1
fi

echo "Waiting for instance boot: $instance_id"
aws --profile $PROFILE --region us-east-1 --output \
    text ec2 wait instance-running --instance-id $instance_id

aws --profile $PROFILE --region us-east-1 --output text ec2 describe-instances --instance-id $instance_id

# Local variables:
# mode: shell-script
# tab-width: 4
# indent-tabs-mode: nil
# end:
