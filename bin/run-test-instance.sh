#!/bin/bash
# Copyright 2017 Noah Meyerhans
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# This script can be used to launch an instance for testing. It's
# mostly just a convenience to avoid having to type in a lot of
# command-line boilerplate.

bin_dir=$(dirname $0)
share_dir=${SHARE_DIR:-${bin_dir}/../share/templates}

profile=default
region=us-east-1
userdata_file="${share_dir}/test-instance-userdata.yaml"
input_json_file="${share_dir}/debian-test-instance.json"
profile_arg=()

usage() {
    cat <<EOF
$0 [OPTIONS] AMI_ID
OPTION: (defaults in parentheses)
 -p, --profile       Use the given profile for AWS API commands ($profile)
 -r, --region        Run in the given AWS region ($region)
 -u, --userdata      Pass the given ($userdata_file)
 -i, --input_json    Pass the given file as input to run-instances ($input_json_file)
 -t, --instance_type Use the given instance type
 -h, --help          Show usage
EOF
}

command -v jq > /dev/null 2>&1 || {
    echo "jq not found in PATH" >&2
    exit 1
}
command -v aws > /dev/null 2>&1 || {
    echo "aws not found in PATH" >&2
    exit 1
}

TEMP=$(getopt -o p:r:u:i:t:h \
              --long profile:,region:,userdata:,input_json:,instance_type:,help \
              -n "$0" -- "$@")
if [ $? != 0 ] ; then echo "Terminating..." >&2 ; exit 1 ; fi
eval set -- "$TEMP"

while true ; do
    case "$1" in
        -p|--profile)
            profile="$2"; shift 2
            ;;
        -r|--region)
            region="$2"; shift 2
            ;;
        -u|--userdata)
            userdata_file="$2"; shift 2
            ;;
        -i|--input_json)
            input_json_file="$2"; shift 2
            ;;
        -t|--instance_type)
            instance_type="--instance-type $2"; shift 2
            ;;
        -h|--help)
            usage ; shift
            exit 0
            ;;
        --)
            shift ; break
            ;;
    esac
done

ami_id=$1

case "$ami_id" in
    ami-*)
        :
    ;;
    *)
        usage
        exit 1
    ;;
esac

instance_id=$(aws ${profile_arg[*]} \
                  --region $region \
                  ec2 run-instances \
                  --image-id "$ami_id" \
                  $instance_type \
                  --user-data "file://${userdata_file}" \
                  --cli-input-json "file://$input_json_file" \
                  --output json | jq -r .Instances[0].InstanceId)

echo "Instance is $instance_id"
if [ -z "$instance_id" ]; then
    echo "Did not get an instance ID. run-instances invocation error?" >&2
    exit 1
fi

aws --output json ${profile_arg[*]} \
    --query 'Reservations[].Instances[].[InstanceId, LaunchTime, PublicIpAddress, Ipv6Address, ImageId, State.Name]' \
    --region $region ec2 describe-instances --instance-id $instance_id

# Local variables:
# mode: shell-script
# tab-width: 4
# indent-tabs-mode: nil
# end:
