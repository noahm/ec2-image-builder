# Debian FAI Cloud Image Builder

## Synopsis

    $ ./bin/launch-fai-builder.sh -h
    ./bin/launch-fai-builder.sh [OPTIONS]
    OPTIONS:
      -p, --profile            Use the given AWS profile for API calls
      -a, --ami                Override the default builder AMI
      -t, --instance-type      Override the default builder instance type
      -C, --classlist          Override the list of classes used by FAI
      -b, --fai-config-branch  Use the given branch in the FAI config git repo
      -h, --help               Show usage

The built-in class list is appropriate for building EC2 stretch images
and will need to be overridden if building for other releases or cloud
services.

The FAI branch defaults to `master`, but should normally be overridden
to `debian/stretch` when building stretch AMIs.

## Description

This tool is intended to be used to generate official and unofficial
(Derivative, Custom, etc) Debian AMIs for use on AWS.

In order to accomplish this, the tool performs the following steps:

1. Launch an EC2 instance with the following properties:
  * Instance has a public IP mapped to it.
  * Instance's security group permits ssh access.
  * Instance is configured with a cloud-init userdata script (see below).
  * Instance has a secondary EBS volume attached.
2. Run FAI to generate a disk image.
3. 'dd' the disk image to the attached EBS volume.
4. Snapshot the EBS volume.
5. Register the snapshot as an AMI.
6. Perform validation steps on the AMI.
7. Publish the AMI to supported AWS regions.
8. Mark the AMI as public.

The workflow may split AMI creation, validation, and publication into
discrete steps if we want to require manual confirmation before proceding.

## Configuration

Configuration of this tool is performed via configuration files; there are
no command-line options. Configuration files use the YAML syntax and
consist of three sections:

1. AWS Global configuration:
  * What AWS profile to use?
  * What region to run it?
2. Instance configuration:
  * Security group and subnet.
  * ssh keypair name
  * Instance type
  * AMI ID
3. Image configuration:
  * Git repository and commit ID for FAI configuration.
  * FAI class list.

## See also

FAI Cloud Images: https://salsa.debian.org/cloud-team/fai-cloud-images

## Author

Noah Meyerhans <noahm@debian.org>

## Copyright

Copyright 2017 Noah Meyerhans

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
