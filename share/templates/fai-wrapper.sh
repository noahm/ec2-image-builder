#!/bin/sh
# Copyright 2017 Noah Meyerhans
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# This script can be used to launch an instance for testing. It's
# mostly just a convenience to avoid having to type in a lot of
# command-line boilerplate.

# defaults configuration
fai_config_repo=https://salsa.debian.org/cloud-team/debian-cloud-images.git
fai_config_branch=master
case $(dpkg --print-architecture) in
    arm64)
        fai_class_list=DEBIAN,STRETCH,ARM64,EXTRAS,CLOUD,EC2,GRUB_EFI_ARM64
        ;;
    amd64)
        fai_class_list=DEBIAN,STRETCH,AMD64,GRUB_PC,EXTRAS,CLOUD,EC2
        ;;
    *)
        echo "Unknown architecture" >&2
        exit 1
        ;;
esac

# Identify the block device representing the target EBS volume
blockdev=
for d in /dev/xvdb /dev/nvme1n1; do
    if [ -b "$d" ]; then
        blockdev=$d
        break
    fi
done
if [ -z "$blockdev" ]; then
    echo "Unable to identify block device" >&2
    exit 1
fi

# override configuration in /srv/fai/fai.env
if [ -f /srv/fai/fai.env ]; then
    . /srv/fai/fai.env
fi

set -e

mkdir -p /srv/fai
cd /srv/fai
git clone --branch "$fai_config_branch" "$fai_config_repo" config

fai-diskimage -u fai-debian-image -S8G --cspace /srv/fai/config/config_space \
              -c ${fai_class_list} /tmp/fai-debian-image.raw
if grep -q FAILED /var/log/fai/fai-debian-image/last/status.log; then
    echo "FAI failures detected" >&2
    exit 1
else
    dd if=/tmp/fai-debian-image.raw "of=$blockdev" bs=512k
fi

# Local variables:
# mode: shell-script
# tab-width: 4
# indent-tabs-mode: nil
# end:
