#!/bin/bash
# Copyright 2019 Noah Meyerhans
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# This script can be used to launch an instance for testing. It's
# mostly just a convenience to avoid having to type in a lot of
# command-line boilerplate.
#
# Given the name of an AMI (not its ID), this script will mark the
# named AMI as public in all standard regions.
# The snapshot backing this AMI's root volume will also be marked
# public, allowing users to clone it for creation of their own
# derivitive AMIs.

usage() {
cat <<EOF
launch-grant [OPTIONS] AMI_NAME
OPTIONS:
 -s, --skip-regions Skip the given regions
 -r, --do-regions   Process AMIs only in the given regions
 -y, --yes          Disable dry-run mode
 -h, --help         Show help

Account IDs should be placed in ./AMI_NAME.txt, where AMI_NAME matches
the AMI_NAME provided.
EOF
}

dry_run_flag=--dry-run
declare -A skip_regions do_regions
skip_regions=()

TEMP=$(getopt -o s:r:yh \
	      --long skip-regions:,do-regions:,yes,help \
	      -n "launch-grant" -- "$@")
eval set -- "$TEMP"

while true; do
    case "$1" in
	-s|--skip-regions)
	    for r in $(echo "$2" | tr ',' ' '); do
		skip_regions+=([$r]=1)
	    done
	    shift 2
	    ;;
	-r|--do-regions)
	    for r in $(echo "$2" | tr ',' ' '); do
		do_regions+=([$r]=1)
	    done
	    shift 2
	    ;;
	-h|--help)
	    usage ; shift ; exit 0
	    ;;
	-y|--yes)
	    dry_run_flag="--no-dry-run"
	    set -e
	    shift
	    ;;
	--)
	    shift ; break
	    ;;
    esac
done

ami_name=$1
if [ -z "$ami_name" ]; then
    echo "usage error" >&2
    exit 1
fi

if [ -n "$AWS_PROFILE" ]; then
    echo "Using profile $AWS_PROFILE"
fi

awscmd="aws --output json"

account_list="$(awk '{print $1}' ${ami_name}.txt)"
if [ -z "$account_list" ]; then
    echo "Did not find any accounts." >&2
    exit 0
fi

if [ "${#do_regions[@]}" -eq 0 ]; then
    for r in $($awscmd ec2 describe-regions | jq -r .Regions[].RegionName); do
	do_regions+=([$r]=1)
    done
fi

echo "Processing regions ${!do_regions[*]}"

for region in "${!do_regions[@]}" ; do
    if [ -n "${skip_regions[$region]}" ]; then
	echo "Skipping $region"
	continue
    fi
    metadata_file=$(mktemp) || exit 1
    $awscmd --region $region ec2 describe-images --owner self \
	    --filter=Name=name,Values="$ami_name" > "$metadata_file"
    ami_id=$(cat "$metadata_file" | jq -r '.Images[0].ImageId')
    snapshot_id=$(cat "$metadata_file" | jq -r '.Images[0].BlockDeviceMappings[0].Ebs.SnapshotId')
    rm "$metadata_file"
    if [ "$ami_id" == "null" ]; then
	echo "NOTICE: No such AMI $ami_name in $region" >&2
	continue
    fi
    if [ "$snapshot_id" == "null" ]; then
	echo "NOTICE: Could not identify snapshot backing $ami_name in $region" >&2
	continue
    fi

    echo "Granting access to $ami_id in $region"
    command="$awscmd --region $region ec2 modify-image-attribute $dry_run_flag --image-id $ami_id \
	    --attribute launchPermission --operation-type add --user-ids $account_list"
    echo "EXEC $command"
    $command

    echo "Granting access to $snapshot_id in $region"
    command="$awscmd --region $region ec2 modify-snapshot-attribute --snapshot-id $snapshot_id \
	    --attribute createVolumePermission $dry_run_flag \
	    --operation add --user-ids $account_list"
    echo "EXEC $command"
    $command

done

