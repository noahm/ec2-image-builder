#!/bin/bash
# Copyright 2017 Noah Meyerhans
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# This script can be used to launch an instance for testing. It's
# mostly just a convenience to avoid having to type in a lot of
# command-line boilerplate.

declare -A skip_regions
dry_run_mode=t

usage() {
    cat <<EOF
copy-to-regions [OPTIONS] SOURCE_REGION SOURCE_AMI_ID
OPTIONS:
 -s, --skip-regions Skip copying to the given list of regions
 -r, --do-regions   Only copy to the given list of regions
 -y, --yes          Disable dry-run mode
 -h, --help         Show usage
EOF
}

dry_run_flag=--dry-run
declare -A skip_regions
declare -a do_regions
skip_regions=()

TEMP=$(getopt -o s:r:yh \
	      --long skip-regions:,do-regions:,yes,help \
	      -n "launch-grant" -- "$@")
eval set -- "$TEMP"

while true; do
    case "$1" in
	-s|--skip-regions)
	    for r in $(echo "$2" | tr ',' ' '); do
		skip_regions+=([$r]=1)
	    done
	    shift 2
	    ;;
	-r|--do-regions)
	    for r in $(echo "$2" | tr ',' ' '); do
		do_regions+=($r)
	    done
	    shift 2
	    ;;
	-h|--help)
	    usage ; shift ; exit 0
	    ;;
	-y|--yes)
	    dry_run_flag="--no-dry-run"
	    dry_run_mode=
	    set -e
	    shift
	    ;;
	--)
	    shift ; break
	    ;;
    esac
done

source_region=$1
source_ami_id=$2

if [ -z "$source_region" ] ||
   [ -z "$source_ami_id" ]; then
    echo "usage error" >&2
    usage
    exit 1
fi

# Don't copy to the source...
skip_regions+=([$source_region]=1)

awscmd="aws"

ami_name=$($awscmd --output json --region $source_region ec2 describe-images --image-id $source_ami_id | jq -r '.Images[].Name')
ami_desc=$($awscmd --output json --region $source_region ec2 describe-images --image-id $source_ami_id | jq -r '.Images[].Description')

echo "Copying $ami_name ($ami_desc)"

if [ -z "${do_regions[*]}" ]; then
    for r in $($awscmd ec2 describe-regions | jq -r .Regions[].RegionName); do
	do_regions+=("$r")
    done
fi

for region in "${do_regions[@]}"; do
    echo "Copying to $region"
    if [ -n "${skip_regions[$region]}" ]; then
	echo "Skipping destination $region"
	continue
    fi
    command="$awscmd --region $region ec2 copy-image \
$dry_run_flag --source-region $source_region \
--source-image-id $source_ami_id --name $ami_name"

    if [ -z "$dry_run_mode" ]; then
	new_id=$($command | jq -r '.ImageId')
    else
	$command
	new_id=ami-dry-run-dummy
    fi
    echo $region: $new_id
done

