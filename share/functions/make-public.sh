#!/bin/bash
# Copyright Noah Meyerhans <frodo@morgul.net>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# This script can be used to launch an instance for testing. It's
# mostly just a convenience to avoid having to type in a lot of
# command-line boilerplate.
#
# Given the name of an AMI (not its ID), this script will mark the
# named AMI as public in all standard regions.
# The snapshot backing this AMI's root volume will also be marked
# public, allowing users to clone it for creation of their own
# derivitive AMIs.

profile=${AWS_PROFILE:-default}

usage() {
    cat <<EOF
make-public [OPTIONS] IMAGE_NAME
OPTIONS:
 -s, --skip-regions Skip processing the named regions (comma-separated)
 -r, --do-regions   Only process the anmed regions (comma-separated)
 -y, --yes          Actually perform mutating operations
 -h, --help         Show usage
EOF
}



dry_run_flag="--dry-run"
declare -a do_regions
declare -A skip_regions
skip_regions=()

TEMP=$(getopt -o s:r:yh \
	      --long skip-regions:,do-regions:,help,yes \
	      -n "make-public" -- "$@")
eval set -- "$TEMP"

while true; do
    case "$1" in
	-s|--skip-regions)
	    for r in $(echo "$2" | tr ',' ' '); do
                skip_regions+=([$r]=1)
            done
            shift 2
            ;;
	-r|--do-regions)
            for r in $(echo "$2" | tr ',' ' '); do
                do_regions+=($r)
            done
            shift 2
            ;;
	-y|--yes)
	    dry_run_flag="--no-dry-run"
	    set -e
	    shift
	    ;;
        -h|--help)
            usage ; shift ; exit 0
            ;;
	--)
	    shift ; break
	    ;;
    esac
done

ami_name=$1
if [ -z "$ami_name" ]; then
    usage
    exit 1
fi

awscmd="aws --output json --profile $profile"

if [ -z "${do_regions[*]}" ]; then
    for r in $($awscmd ec2 describe-regions | jq -r .Regions[].RegionName); do
        do_regions+=("$r")
    done
fi

for region in ${do_regions[@]} ; do
    if [ -n "${skip_regions[$region]}" ]; then
	echo "Skipping $region"
	continue
    fi
    metadata_file=$(mktemp) || exit 1
    $awscmd --region $region ec2 describe-images --owner self \
	    --filter=Name=name,Values="$ami_name" > "$metadata_file"
    ami_id=$(cat "$metadata_file" | jq -r '.Images[0].ImageId')
    snapshot_id=$(cat "$metadata_file" | jq -r '.Images[0].BlockDeviceMappings[0].Ebs.SnapshotId')
    rm "$metadata_file"
    if [ "$ami_id" == "null" ]; then
	echo "NOTICE: No such AMI $ami_name in $region" >&2
	continue
    fi
    if [ "$snapshot_id" == "null" ]; then
	echo "NOTICE: Could not identify snapshot backing $ami_name in $region" >&2
	continue
    fi

    echo "Marking $ami_id public in $region"
    $awscmd --region $region ec2 modify-image-attribute $dry_run_flag \
	    --image-id $ami_id --launch-permission "{\"Add\": [{\"Group\":\"all\"}]}"

    echo "Marking $snapshot_id public in $region"
    $awscmd --region $region ec2 modify-snapshot-attribute --snapshot-id "$snapshot_id" \
	    --attribute createVolumePermission \
	    --operation add \
	    --group-names all \
	    $dry_run_flag
done

