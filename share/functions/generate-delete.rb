#!/usr/bin/env ruby
# Copyright 2017 Noah Meyerhans
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# This script can be used to launch an instance for testing. It's
# mostly just a convenience to avoid having to type in a lot of
# command-line boilerplate.

require 'json'
x = JSON.load($stdin)
x.each do |region|
  if region.length != 3
    puts "Skipping #{region[0]}"
  else
    r=region[0]
    ami=region[1]
    snap=region[2]
    puts "aws --profile debian --region #{r} ec2 deregister-image --image-id #{ami}"
    puts "aws --profile debian --region #{r} ec2 delete-snapshot --snapshot-id #{snap}"
  end
end

# Local variables:
# mode: ruby
# tab-width: 4
# indent-tabs-mode: nil
# end:
