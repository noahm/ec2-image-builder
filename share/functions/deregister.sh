#!/bin/bash
# Copyright 2017 Noah Meyerhans
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# This script can be used to launch an instance for testing. It's
# mostly just a convenience to avoid having to type in a lot of
# command-line boilerplate.

usage() {
    cat <<EOF
$(basename $0) [OPTIONS] IMAGE_NAME
OPTIONS:
 -s, --skip-regions Skip processing the named regions (comma-separated)
 -r, --do-regions   Only process the anmed regions (comma-separated)
 -y, --yes          Actually perform mutating operations
 -h, --help         Show usage
EOF
}

dry_run_flag="--dry-run"
declare -a do_regions
declare -A skip_regions
skip_regions=()

TEMP=$(getopt -o s:r:yh \
	      --long skip-regions:,do-regions:,help,yes \
	      -n "make-public" -- "$@")
eval set -- "$TEMP"

while true; do
    case "$1" in
	-s|--skip-regions)
	    for r in $(echo "$2" | tr ',' ' '); do
                skip_regions+=([$r]=1)
            done
            shift 2
            ;;
	-r|--do-regions)
            for r in $(echo "$2" | tr ',' ' '); do
                do_regions+=($r)
            done
            shift 2
            ;;
	-y|--yes)
	    dry_run_flag="--no-dry-run"
	    set -e
	    shift
	    ;;
        -h|--help)
            usage ; shift ; exit 0
            ;;
	--)
	    shift ; break
	    ;;
    esac
done

image_name=$1
profile=${AWS_PROFILE:-default}
awscmd="aws --output json --profile $profile"

test -n "$image_name" || {
    echo "Need an image name." >&2
    exit 1
}

if [ -z "${do_regions[*]}" ]; then
    for r in $($awscmd ec2 describe-regions | jq -r .Regions[].RegionName); do
        do_regions+=("$r")
    done
fi

for region in ${do_regions[@]} ; do
    if [ -n "${skip_regions[$region]}" ]; then
	    echo "Skipping $region"
	    continue
    fi
    json=
    ami=
    snap=
    json=$($awscmd --region $region ec2 describe-images \
                   --owner self \
                   --filter=Name=name,Values=$image_name)
    ami=$(echo "$json" | jq -r '.Images[0].ImageId')
    snap=$(echo "$json" | jq -r '.Images[0].BlockDeviceMappings[0].Ebs.SnapshotId')
    if [[ "$ami" = ami-* ]] &&
           [[ "$snap" = snap-* ]]; then
        $awscmd --region $region ec2 deregister-image --image-id $ami $dry_run_flag
        $awscmd --region $region ec2 delete-snapshot --snapshot-id $snap $dry_run_flag
    else
        echo "Did not find $image_name in $region" >&2
    fi
done

# Local variables:
# mode: shell-script
# tab-width: 4
# indent-tabs-mode: nil
# end:
