#!/bin/bash

region=us-east-1

usage() {
    cat <<EOF
amis [OPTIONS]
OPTIONS:
 -r, --region Specify the region in which to list AMIs ($region)
 -h, --help   Show usage
EOF
}

optstr=r:h
longoptstr=regions:,help

TEMP=$(getopt -o $optstr \
	      --long $longoptstr \
	      -n "amis" -- "$@")
eval set -- "$TEMP"

while true; do
    case "$1" in
	-r|--regions)
	    region=$2 ; shift 2
	    ;;
	-h|--help)
	    usage ; shift
	    exit 0
	    ;;
	--)
	    shift ; break
	    ;;
	*)
	    echo "Internal error" ; exit 1
	    ;;
    esac
done

aws --region $region --output json ec2 describe-images --owner self \
    | jq '.Images[] | .ImageId, .Name'

